<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rulers Model
 *
 * @property \App\Model\Table\PeriodsTable|\Cake\ORM\Association\BelongsTo $Periods
 * @property \App\Model\Table\DynastiesTable|\Cake\ORM\Association\BelongsTo $Dynasties
 * @property \App\Model\Table\DatesTable|\Cake\ORM\Association\HasMany $Dates
 *
 * @method \App\Model\Entity\Ruler get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ruler newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ruler[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ruler|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ruler|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ruler patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ruler[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ruler findOrCreate($search, callable $callback = null, $options = [])
 */
class RulersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rulers');
        $this->setDisplayField('ruler');
        $this->setPrimaryKey('id');

        $this->belongsTo('Periods', [
            'foreignKey' => 'period_id'
        ]);
        $this->belongsTo('Dynasties', [
            'foreignKey' => 'dynasty_id'
        ]);
        $this->hasMany('Dates', [
            'foreignKey' => 'ruler_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('sequence')
            ->allowEmpty('sequence');

        $validator
            ->scalar('ruler')
            ->maxLength('ruler', 100)
            ->allowEmpty('ruler')
            ->add('ruler', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->isUnique(['ruler']));
        $rules->add($rules->existsIn(['period_id'], 'Periods'));
        $rules->add($rules->existsIn(['dynasty_id'], 'Dynasties'));

        return $rules;
    }
}
