<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading[]|\Cake\Collection\CollectionInterface $signReadings
 */
?>
<div class="signReadings content">
    <h3><?= __('Preferred Readings') ?></h3>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead align="left">
                <tr>
                	<th><?= $this->Paginator->sort('Id') ?></th>
                    <th><?= $this->Paginator->sort('sign_reading') ?></th>
                    <th><?= $this->Paginator->sort('sign_name') ?></th>
                    <th><?= $this->Paginator->sort('meaning') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($signReadings as $signReading): ?>
                    <?php if($signReading->preferred_reading==1):?>
                <tr> 
                	<td align="left"><?= h($signReading->id) ?></td>
                    <td align="left"><a href='view_preferred/<?=h($signReading->id)?>'><?= h($signReading->sign_reading) ?></a></td>
                    <td align="left"><?= h($signReading->sign_name) ?></td>
                    <td align="left"><?= h($signReading->meaning) ?></td>
                </tr>
            <?php endif?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
