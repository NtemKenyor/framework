<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * MaterialAspects Controller
 *
 * @property \App\Model\Table\MaterialAspectsTable $MaterialAspects
 *
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialAspectsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialAspects = $this->paginate($this->MaterialAspects);

        $this->set(compact('materialAspects'));
    }

    /**
     * View method
     *
     * @param string|null $id Material Aspect id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialAspect = $this->MaterialAspects->get($id, [
            'contain' => ['ArtifactsMaterials']
        ]);

        $this->set('materialAspect', $materialAspect);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialAspect = $this->MaterialAspects->newEntity();
        if ($this->request->is('post')) {
            $materialAspect = $this->MaterialAspects->patchEntity($materialAspect, $this->request->getData());
            if ($this->MaterialAspects->save($materialAspect)) {
                $this->Flash->success(__('The material aspect has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The material aspect could not be saved. Please, try again.'));
        }
        $this->set(compact('materialAspect'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Material Aspect id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialAspect = $this->MaterialAspects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $materialAspect = $this->MaterialAspects->patchEntity($materialAspect, $this->request->getData());
            if ($this->MaterialAspects->save($materialAspect)) {
                $this->Flash->success(__('The material aspect has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The material aspect could not be saved. Please, try again.'));
        }
        $this->set(compact('materialAspect'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Material Aspect id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $materialAspect = $this->MaterialAspects->get($id);
        if ($this->MaterialAspects->delete($materialAspect)) {
            $this->Flash->success(__('The material aspect has been deleted.'));
        } else {
            $this->Flash->error(__('The material aspect could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
