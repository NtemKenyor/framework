<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 *
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $externalResources = $this->paginate($this->ExternalResources);

        $this->set(compact('externalResources'));
    }

    /**
     * View method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $externalResource = $this->ExternalResources->get($id, [
            'contain' => []
        ]);

        $artifacts = $this->loadModel('artifacts_external_resources');
        $count = $artifacts->find('list', ['conditions' => ['external_resource_id' => $id]])->count();

        $this->set(compact('externalResource', 'count'));
    }
}
