<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsPublicationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsPublicationsTable Test Case
 */
class ArtifactsPublicationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsPublicationsTable
     */
    public $ArtifactsPublications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts_publications',
        'app.artifacts',
        'app.publications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactsPublications') ? [] : ['className' => ArtifactsPublicationsTable::class];
        $this->ArtifactsPublications = TableRegistry::getTableLocator()->get('ArtifactsPublications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactsPublications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
